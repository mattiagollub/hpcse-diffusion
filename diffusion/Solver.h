//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 2 - Diffusion       											//
//																				//
//		Cpyright © 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#ifndef __SOLVER_H
#define __SOLVER_H

#include "Commons.h"
#include "System.h"

#include <fstream>

using namespace std;

//==============================================================================//
//		Solver class. Base class for every kind of solver.						//
//==============================================================================//

class Solver
{
public:

    //==========================================================================//
    //	Constructor / Destructor.                          						//
    //==========================================================================//

    Solver(const char* fileName);
    virtual ~Solver();

    //==========================================================================//
    //	Solver methods.                                   						//
    //==========================================================================//

    void Advance(System& s, size_t steps, bool plotErrors);

protected:
    virtual void advanceOneStep(System& s) = 0;

private:
   	ofstream file;
    const char* fileName;

};

#endif
