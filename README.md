# HPCSE Project 2 - Diffusion.

Developed as part of the High Performance Computing for CSE class, the project demonstrates an effiecient simulation of heat diffusion in a medium. The software is written in C++, uses OpenMP for multi-core acceleration and is configured to run on ETH's cluster Brutus.