#!/bin/bash

mkdir -p results

export OMP_NUM_THREADS=48
bsub -R "select[model==Opteron6174]" -n 48 -W 00:20 < run.sh
