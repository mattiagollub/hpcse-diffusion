//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 2 - Diffusion       											//
//																				//
//		Cpyright © 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#include "Solver.h"

//==============================================================================//
//		Solver class implementation.                    						//
//==============================================================================//

Solver::Solver(const char* fileName)
{
    this->fileName = fileName;
    file.open(fileName, ios::out);

	if (!file.good())
		LOG("Unable to write to file: %s\n", fileName);

	file.close();
}

Solver::~Solver()
{
	if (file.is_open())
		file.close();
}

void Solver::Advance(System& s, size_t steps, bool plotErrors)
{
    for (size_t step = 0; step < steps; step++)
    {
        // Simulate one step
        //====================
        advanceOneStep(s);

        // Advance time
        //===============
        s.t += s.dt;

        // Plot errors
        //==============
        if (plotErrors)
        {
            file.open(fileName, ios::out | ios::app);

            if (!file.good())
            {
                LOG("Unable to write to file: %s\n", fileName);
                file.close();
                return;
            }

            file << s.Linf() << " " << s.L1() << " " << s.L2() << "\n";

            file.close();
        }
    }
}
