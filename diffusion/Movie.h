//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 2 - Diffusion       											//
//																				//
//		Cpyright © 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#ifndef __MOVIE_H
#define __MOVIE_H

#include "Commons.h"
#include "System.h"

#include <fstream>

using namespace std;

//==============================================================================//
//		Result visualization.													//
//==============================================================================//

class Movie
{
public:

	//==========================================================================//
	//	Constructor / Destructor.												//
	//==========================================================================//

	Movie(const char* fileName, System& firstFrame, bool save);
	~Movie();

	//==========================================================================//
	//	Movie managment.														//
	//==========================================================================//

	void AddFrame(System& status);

private:
	ofstream file;
	const char* fileName;
};

#endif
