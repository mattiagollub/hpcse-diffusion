//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 1 - The N-body problem											//
//																				//
//		Cpyright © 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#include "Movie.h"

//==============================================================================//
//		Movie class implementation.								    			//
//==============================================================================//

Movie::Movie(const char* fileName, System& firstFrame, bool save)
{
	this->fileName = fileName;
	file.open(fileName, ios::out);

	if (!file.good())
		LOG("Unable to write to file: %s\n", fileName);

	file << (unsigned int)firstFrame.Width() << "\n";
	file.close();

        if (save)
		AddFrame(firstFrame);
}

Movie::~Movie()
{
	if (file.is_open())
		file.close();
}

void Movie::AddFrame(System& status)
{
	file.open(fileName, ios::out | ios::app);

	if (!file.good())
	{
		LOG("Unable to write to file: %s\n", fileName);
		file.close();
		return;
	}

	for (size_t i = 0; i < status.Width(); i++)
	{
        for (size_t j = 0; j < status.Width(); j++)
        {
            file << status(i, j) << " ";
        }

        file << "\n";
	}

	file.close();
}
