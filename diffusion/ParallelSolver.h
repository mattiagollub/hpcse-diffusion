//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 2 - Diffusion       											//
//																				//
//		Cpyright © 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#ifndef __PARALLELSOLVER_H
#define __PARALLELSOLVER_H

#include "Commons.h"
#include "System.h"
#include "Solver.h"

//==============================================================================//
//		ParallelSolver class. OpenMP solver for the diffusion problem.			//
//==============================================================================//

class ParallelSolver : public Solver
{
public:

    //==========================================================================//
    //	Constructor / Destructor.                          						//
    //==========================================================================//

    ParallelSolver(const char* fileName, size_t n);
    ~ParallelSolver();

    //==========================================================================//
    //	ParallelSolver methods.                            						//
    //==========================================================================//

protected:
    void advanceOneStep(System& s);
    size_t nThreads;
};

#endif
