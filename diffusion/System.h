//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 2 - Diffusion       											//
//																				//
//		Cpyright © 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#ifndef __SYSTEM_H
#define __SYSTEM_H

#include "Commons.h"

typedef float (*initFunc)(const float& x, const float& y);

//==============================================================================//
//		System class. Represents the state of the system.						//
//==============================================================================//

class System
{
public:

    //==========================================================================//
    //	Constructor / Destructor.                          						//
    //==========================================================================//

    System(size_t gridWidth, float dt, initFunc bc, float d);
    ~System();

    //==========================================================================//
    //	Fields access.                                   						//
    //==========================================================================//

    float& operator()(size_t i, size_t j);
    const float& operator()(size_t i, size_t j) const;

    size_t Width();
    float D;
    float t;
    float dt;
    float dx;

    //==========================================================================//
    //	Error measures.                                 						//
    //==========================================================================//

    float Linf();
    float L1();
    float L2();

private:

    //==========================================================================//
    //	Analytic solution.                                 						//
    //==========================================================================//

    float _analyticSolution(float x, float y);

    //==========================================================================//
    //	Private fields.                                 						//
    //==========================================================================//

    std::vector<float>  _data;
    size_t              _width;

};

#endif
