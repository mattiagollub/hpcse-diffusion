//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 2 - Diffusion       											//
//																				//
//		Cpyright © 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#include "Vectors.h"

//==============================================================================//
//		Solve tridiagonal system. Implementation taken from the problem sheet.	//
//==============================================================================//

void solveTridiagonal(fvec& a, fvec& b, fvec& c, fvec& rhs, System& s, direction dir, size_t f)
{
    size_t n = rhs.size();

    // Boundary don't need to be calculated, they are forced to 0
    if (f == 0 || f == n-1)
        return;

    for (size_t i = 1; i < n-1; i++)
    {
        float m = a[i] / b[i-1];
        b[i] = b[i] - m*c[i-1];
        rhs[i] = rhs[i] - m*rhs[i-1];
    }

    if (dir == row)
    {
        s(n-2, f) = rhs[n-2] / b[n-2];

        for (size_t i = n-3; i >= 1; i--)
        {
            s(i, f) = (rhs[i] - c[i] * s(i+1, f)) / b[i];
        }
    }
    else
    {
        s(f, n-2) = rhs[n-2] / b[n-2];

        for (size_t i = n-3; i >= 1; i--)
        {
            s(f, i) = (rhs[i] - c[i] * s(f, i+1)) / b[i];
        }
    }
}
