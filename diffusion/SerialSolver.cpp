//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 2 - Diffusion       											//
//																				//
//		Cpyright © 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#include "SerialSolver.h"
#include "Vectors.h"

//==============================================================================//
//		SerialSolver class implementation.                          			//
//==============================================================================//

SerialSolver::SerialSolver(const char* fileName)
    : Solver(fileName)
{
    // Nothing to do here
}

SerialSolver::~SerialSolver()
{
    // Nothing to do here
}

void SerialSolver::advanceOneStep(System& s)
{
     size_t n = s.Width();
    float dx = s.dx;
    float dt = s.dt;

    // Precompute coefficients
    float ca = 1.0f / (dx * dx);
    float cb = 2.0f / (s.D * dt) - 2.0f / (dx * dx);

    //==========================================================================//
    //		Step 1: Row-wise.                                        			//
    //==========================================================================//

    vector<vector<float> > rhss = vector<vector<float> >(n, vector<float>(n, 0.0f));

    // Compute right-hand sides
    for (size_t j = 0; j < n; j++)
    {
        for (size_t i = 0; i < n; i++)
        {
            float tmp = 0.0f;
            if (j > 0)
            {
                tmp += ca * s(i, j-1);
            }

            tmp += cb * s(i, j);

            if (j < n-1)
            {
                tmp += ca * s(i, j+1);
            }

            rhss[j][i] += tmp;
        }
    }

    // Solve every sigle row separately
    for (size_t j = 0; j < n; j++)
    {
        // Create tridiagonal matrix
        vector<float> a = vector<float>(n, 1.0f / (dx * dx));
        vector<float> b = vector<float>(n, -2.0f / (s.D * dt) - 2.0f / (dx * dx));
        vector<float> c = vector<float>(n, 1.0f / (dx * dx));

        // Solve row
        solveTridiagonal(a, b, c, rhss[j], s, row, j);
    }

    //==========================================================================//
    //		Step 2: Column-wise.                                       			//
    //==========================================================================//

    rhss = vector<vector<float> >(n, vector<float>(n, 0.0f));

    // Compute right-hand sodes
    for (size_t i = 0; i < n; i++)
    {
        for (size_t j = 0; j < n; j++)
        {
            float tmp = 0.0f;
            if (i > 0)
            {
                tmp += ca * s(i-1, j);
            }

            tmp += cb * s(i, j);

            if (i < n-1)
            {
                tmp += ca * s(i+1, j);
            }

            rhss[i][j] += tmp;
        }
    }

    for (size_t i = 0; i < n; i++)
    {
        // Create tridiagonal matrix
        fvec a = fvec(n, 1.0f / (dx * dx));
        fvec b = fvec(n, -2.0f / (s.D * dt) - 2.0f / (dx * dx));
        fvec c = fvec(n, 1.0f / (dx * dx));

        // Solve column
        solveTridiagonal(a, b, c, rhss[i], s, column, i);
    }
}
