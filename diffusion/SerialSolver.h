//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 2 - Diffusion       											//
//																				//
//		Cpyright © 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#ifndef __SERIALSOLVER_H
#define __SERIALSOLVER_H

#include "Commons.h"
#include "System.h"
#include "Solver.h"

//==============================================================================//
//		SerialSolver class. Serial solver for the diffusion problem.			//
//==============================================================================//

class SerialSolver : public Solver
{
public:

    //==========================================================================//
    //	Constructor / Destructor.                          						//
    //==========================================================================//

    SerialSolver(const char* fileName);
    ~SerialSolver();

    //==========================================================================//
    //	SerialSolver methods.                              						//
    //==========================================================================//

protected:
    void advanceOneStep(System& s);
};

#endif
