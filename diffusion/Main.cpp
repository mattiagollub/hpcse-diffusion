//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 2 - Diffusion       											//
//																				//
//		Cpyright © 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#include <stdio.h>
#include <fstream>

#include "Commons.h"
#include "Solver.h"
#include "SerialSolver.h"
#include "ParallelSolver.h"
#include "System.h"
#include "ArgumentParser.h"
#include "Movie.h"
#include "Timer.h"
#include "omp.h"

using namespace std;

//==============================================================================//
//		Boundary conditions function.                   						//
//==============================================================================//

float bc(const float& x, const float& y)
{
    return sin(x * 2.0f * PI) * sin(y * 2.0f * PI);
}

//==============================================================================//
//		Entry point.                                    						//
//==============================================================================//

int main(int argc, char* argv[])
{

	LOG("ETHZ - High Performance Computing for CSE I\n");
	LOG("Project 2 - Diffusion\n\n");

	LOG("Mattia Gollub (gollubm@student.ethz.ch)\n\n");

    //	Read arguments
    //==================
	ArgumentParser args(argc, argv);
    const size_t width = (size_t)args("-w").asInt(20);
    const float dt = (float)args("-dt").asDouble(-1.0);
    const float d = (float)args("-d").asDouble(1.0f);
    const float time = args("-t").asDouble(0.1);
    const bool plotErrors = args("-e").asBool(true);
    const bool saveResult = args("-r").asBool(true);
    const std::string solver = args("-s").asString("parallel");
    const std::string output = args("-od").asString("result.dat");
    const std::string errors = args("-oe").asString("errors.dat");
    const std::string timings = args("-ot").asString("timings.dat");
    const size_t n = (size_t)args("-n").asInt(4);

    Solver* s = NULL;
    if (solver.compare("serial") == 0)
    {
        s = new SerialSolver(errors.c_str());
        LOG("Using serial solver.\n");
    }
    if (solver.compare("parallel") == 0)
    {
        s = new ParallelSolver(errors.c_str(), n);
        LOG("Using parallel solver with %u cores.\n", (unsigned int)n);
    }

    if (s == NULL)
        LOG("Invalide solver: %s\n", solver.c_str());

    // Initialize system
    //====================
    System system(width, dt, &bc, d);
    Movie m(output.c_str(), system, saveResult);


    Timer timer;
    timer.start();

    // Run simulation
	while (system.t < time)
	{
		s->Advance(system, 1, plotErrors);
		if (saveResult)
			m.AddFrame(system);
	}

    float runningTime = timer.stop();

    ofstream file;
    file.open(timings.c_str(), ios::out);
    file << runningTime << "\n";
    file.close();

	LOG("Simulation complete.\n");
    SAFE_DEL(s);

	return 0;
}
