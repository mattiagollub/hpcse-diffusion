% MATLAB script for plotting scaling

% Make strong scaling plot
%================================

sizes = [1, 2, 3, 6, 12, 24, 48];
samples = size(sizes, 2);
times = zeros(1, samples);

for sid=1:samples

	% Get filename
	s = sizes(sid);
	fileName = sprintf('results/ss_timings_%u.dat', s);
	fileID = fopen(fileName);

	% Read result file
	A = fscanf(fileID, '%f');
	times(1, sid) = A(1, 1);
	
	% Close file
	fclose(fileID);
end

% Compute speedup
S = times(1, 1) * ones(1, samples) ./ times(1, :);

% Save plot
figure, close;
plot(sizes, sizes, 'r--', sizes, S, 'b-o');
title('Strong Scaling. -w 2000');
xlabel('Number of cores');
ylabel('Speedup');
legend('ideal', 'measured');
set(gcf, 'PaperUnits', 'centimeters');
set(gcf, 'PaperSize', [16 11]);
set(gcf, 'PaperPosition', [0 0 15 10]);
saveas(gcf, 'results/strong_scaling.pdf');
%print -dpdf -r600 results/strong_scaling.pdf

% Make weak scaling plot
%================================

sizes = [1, 2, 3, 6, 12, 24, 48];
samples = size(sizes, 2);
times = zeros(1, samples);

for sid=1:samples

	% Get filename
	s = sizes(sid);
	fileName = sprintf('results/ws_timings_%u.dat', s);
	fileID = fopen(fileName);

	% Read result file
	A = fscanf(fileID, '%f');
	times(1, sid) = A(1, 1);
	
	% Close file
	fclose(fileID);
end

% Compute efficiency
E = times(1, 1) * ones(1, samples) ./ times(1, :);

% Save plot
figure, close;
plot(sizes, ones(1, samples), 'r--', sizes, E, 'b-o');
title('Weak Scaling. Varying grid size.');
xlabel('Number of cores');
ylabel('Efficiency');
legend('ideal', 'measured');
set(gcf, 'PaperUnits', 'centimeters');
set(gcf, 'PaperPosition', [0 0 15 10]);
set(gcf, 'PaperSize', [16 11]);
saveas(gcf, 'results/weak_scaling.pdf');
%print -dpdf -r600 results/weak_scaling.pdf

quit;
