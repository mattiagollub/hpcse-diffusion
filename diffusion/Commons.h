//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 2 - Diffusion       											//
//																				//
//		Cpyright © 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#ifndef __COMMONS_H
#define __COMMONS_H

#include <cmath>
#include <stdio.h>
#include <vector>

using namespace std;

//==============================================================================//
//		Useful macros.															//
//==============================================================================//

#define PI 3.1415926535897932384626433832795
#define E 2.7182818284590452353602874713526

#define SAFE_DEL(ptr) if (ptr != NULL) { delete ptr; ptr = NULL; }
#define SAFE_DELA(ptr) if (ptr != NULL) { delete[] ptr; ptr = NULL; }

#define LOG printf

#endif
