%MATLAB script for plotting results

% Make final movie
%===================

% Get filenames
fileName = 'results/result_final.dat';
movieName = 'results/movie.avi';
fileID = fopen(fileName);

% Setup movie file
videoObj = VideoWriter(movieName, 'Indexed AVI');
videoObj.Colormap = jet(256);
open(videoObj);

% Read result file
[A, toRead] = fscanf(fileID, '%f');
n = A(1);
toRead = toRead - 1;
frameSize = n*n;
nFrames = toRead / frameSize;
	
A = A(2:end);
	
% Calculate range of values
iMax = max(A(1:end));
iMin = min(A(1:end));
delta = iMax - iMin;

% Generate frames
for i=1:nFrames
	I = reshape(A((i-1)*(n*n)+1:i*n*n), n, n);
	I = uint8((I - iMin)./delta*256);
   
	writeVideo(videoObj, I);
end
	
% Close file
close(videoObj);
fclose(fileID);

% Make spatial convergence plot
%================================

frame = 10;
sizes = [4, 8, 16, 32, 64, 128, 256, 512];
samples = size(sizes, 2);
Linf = zeros(1, samples);
L1 = zeros(1, samples);
L2 = zeros(1, samples);

for sid=1:samples

	% Get filename
	s = sizes(sid);
	fileName = sprintf('results/errors_s_%u.dat', s);
	fileID = fopen(fileName);


	% Read result file
	A = fscanf(fileID, '%f');
	
	% Read norms
	Linf(1,sid) = A((frame - 1) * 3 + 1);
	L1(1,sid) = A((frame - 1) * 3 + 2);
	L2(1,sid) = A((frame - 1) * 3 + 3);

	
	% Close file
	fclose(fileID);
end

% Save plot
figure, close;
loglog(sizes, Linf, 'r-o', sizes, L1, 'g-o', sizes, L2, 'b-o');
title('Spatial Convergence. -dt 0.001 -t 0.01');
xlabel('Grid size');
ylabel('Error norm');
legend('L_\infty', 'L_1', 'L_2');
set(gcf, 'PaperUnits', 'centimeters');
set(gcf, 'PaperSize', [16 11]);
set(gcf, 'PaperPosition', [0 0 15 10]);
saveas(gcf, 'results/spatial_convergence.pdf');
%print -dpdf -r600 results/spatial_convergence.pdf

% Make temporal convergence plot
%=================================

times = [0.01, 0.005, 0.0025, 0.00125, 0.000625, 0.0003125, 0.00015625];
samples = size(times, 2);
Linf = zeros(1, samples);
L1 = zeros(1, samples);
L2 = zeros(1, samples);

for tid=1:samples

	% Get filename
	t = times(tid);
	fileName = sprintf('results/errors_t_%.12g.dat', t);
	fileID = fopen(fileName);


	% Read result file
	[A, count] = fscanf(fileID, '%f');
	frame = count / 3;	

	% Read norms
	Linf(1,tid) = A((frame - 1) * 3 + 1);
	L1(1,tid) = A((frame - 1) * 3 + 2);
	L2(1,tid) = A((frame - 1) * 3 + 3);

	
	% Close file
	fclose(fileID);
end

% Save plot
figure, close;
loglog(times, Linf, 'r-o', times, L1, 'g-o', times, L2, 'b-o');
title('Temporal Convergence. -w 20 -t 0.1');
xlabel('Timestep');
ylabel('Error norm');
legend('L_\infty', 'L_1', 'L_2');
set(gcf, 'PaperUnits', 'centimeters');
set(gcf, 'PaperSize', [16 11]);
set(gcf, 'PaperPosition', [0 0 15 10]);
saveas(gcf, 'results/temporal_convergence.pdf');
%print -dpdf -r600 results/temporal_convergence.pdf

% Make general convergence plot
%================================

sizes = [4, 8, 16, 32, 64, 128, 256, 512];
samples = size(sizes, 2);
Linf = zeros(1, samples);
L1 = zeros(1, samples);
L2 = zeros(1, samples);

for sid=1:samples

	% Get filename
	s = sizes(sid);
	fileName = sprintf('results/errors_g_%u.dat', s);
	fileID = fopen(fileName);


	% Read result file
	[A, count] = fscanf(fileID, '%f');
	frame = count / 3;	

	% Read norms
	Linf(1,sid) = A((frame - 1) * 3 + 1);
	L1(1,sid) = A((frame - 1) * 3 + 2);
	L2(1,sid) = A((frame - 1) * 3 + 3);

	
	% Close file
	fclose(fileID);
end

% Save plot
figure, close;
loglog(sizes, Linf, 'r-o', sizes, L1, 'g-o', sizes, L2, 'b-o');
title('General Convergence. -t 0.001');
xlabel('Gridsize');
ylabel('Error norm');
legend('L_\infty', 'L_1', 'L_2');
set(gcf, 'PaperUnits', 'centimeters');
set(gcf, 'PaperSize', [16 11]);
set(gcf, 'PaperPosition', [0 0 15 10]);
saveas(gcf, 'results/general_convergence.pdf');
%print -dpdf -r600 results/general_convergence.pdf

quit;
