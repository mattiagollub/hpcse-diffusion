#!/bin/bash

mkdir -p results

echo "================================================================================="
echo "Starting tests..."
echo "================================================================================="

echo "Strong scaling"
echo "---------------------------------------------------------------------------------"

# Strong scaling
for N in 1 2 3 6 12 24 48
do
	export OMP_NUM_THREADS=${N}
	./diffusion -n ${N} -r false -w 2000 -d 1.0 -dt 0.01 -t 0.001 -s parallel -od results/result_ss_${N}.dat -oe results/errors_ss_${N}.dat -ot results/ss_timings_${N}.dat
done

echo "Weak scaling"
echo "---------------------------------------------------------------------------------"

# Weak scaling
for N in 1,316 2,447 3,547 6,774 12,1095 24,1549 48,2190
do
	IFS=","
	set ${N}
	export OMP_NUM_THREADS=${1}
	./diffusion -n ${1} -r false -w ${2} -d 1.0 -dt 0.01 -t 0.001 -s parallel -od results/result_ws_${1}.dat -oe results/errors_ws_${1}.dat -ot results/ws_timings_${1}.dat
done

echo "Spatial convergence"
echo "---------------------------------------------------------------------------------"

export OMP_NUM_THREADS=4

# Spatial convergenge
for GS in 4 8 16 32 64 128 256 512
do
	./diffusion -w ${GS} -dt 0.001 -n 4 -d 1.0 -t 0.01 -e true -s parallel -od results/result_s_${GS}.dat -oe results/errors_s_${GS}.dat -ot results/_dummy_.dat
	rm results/result_s_${GS}.dat
echo "---------------------------------------------------------------------------------"
done

echo "Temporal convergence"
echo "---------------------------------------------------------------------------------"

# Temporal convergence
for DT in 0.01 0.005 0.0025 0.00125 0.000625 0.0003125 0.00015625
do
	./diffusion -w 20 -dt ${DT} -n 4 -d 1.0 -t 0.1 -e true -s parallel -od results/result_t_${DT}.dat -oe results/errors_t_${DT}.dat -ot results/_dummy_.dat
	rm results/result_t_${DT}.dat
echo "---------------------------------------------------------------------------------"
done

echo "General convergence"
echo "---------------------------------------------------------------------------------"

# General convergence
for GS in 4 8 16 32 64 128 256 512
do
	./diffusion -w ${GS} -n 4 -d 1.0 -t 0.001 -e true -s parallel -od results/result_g_${GS}.dat -oe results/errors_g_${GS}.dat -ot results/_dummy_.dat
	rm results/result_g_${GS}.dat
echo "---------------------------------------------------------------------------------"
done
echo "[DONE]"
echo "================================================================================="

echo "Simulating final movie..."
echo "================================================================================="

# Finally a nice movie
./diffusion -w 100 -d 1.0 -n 4 -t 0.1 -e false -s parallel -od results/result_final.dat -ot results/_dummy_.dat


rm results/_dummy_.dat

echo "[DONE]"
echo "================================================================================="
