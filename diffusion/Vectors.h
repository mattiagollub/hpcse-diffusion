//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 2 - Diffusion       											//
//																				//
//		Cpyright © 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#ifndef __VECTORS_H
#define __VECTORS_H

#include <cmath>
#include <vector>

#include "System.h"

using namespace std;

//==============================================================================//
//		N-elements vector.														//
//==============================================================================//

typedef vector<float> fvec;

enum direction
{
    row,
    column
};

void solveTridiagonal(fvec& a, fvec& b, fvec& c, fvec& rhs, System& s, direction dir, size_t fixedIdx);

#endif
