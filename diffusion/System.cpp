//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 2 - Diffusion       											//
//																				//
//		Cpyright © 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#include "System.h"

using namespace std;

//==============================================================================//
//		system class implementation.                    						//
//==============================================================================//

System::System(size_t gridWidth, float dt, initFunc bc, float d)
    : D(d), t(0.0f), _data(gridWidth * gridWidth), _width(gridWidth)
{
    // Initialize space and stepping
    //================================
    dx = 1.0f / (float)_width;
    if (dt <= 0.0f)
        this->dt = 0.5f / D * dx * dx;
    else
        this->dt = dt;

    // Initialize density distribution
    //==================================
    for (size_t j = 0; j < gridWidth; j++)
    {
        size_t bj= j * gridWidth;
        for (size_t i = 0; i < gridWidth; i++)
        {
            float x = (float)i / (float)gridWidth;
            float y = (float)j / (float)gridWidth;
            _data[bj + i] = bc(x, y);
        }
    }

    // Ensure 0 boundary conditions
    //===============================
    for (size_t i = 0; i < gridWidth; i++)
    {
        // First row
        _data[i] = 0.0f;

        // Last row
        _data[(gridWidth - 1) * gridWidth + i] = 0.0f;

        // First column
        _data[i * gridWidth] = 0.0f;

        // Last column
        _data[i * gridWidth + (gridWidth - 1)] = 0.0f;
    }
}

System::~System()
{
    // Nothing to do here...
}

float& System::operator()(size_t i, size_t j)
{
    return _data[j * _width  + i];
}

const float& System::operator()(size_t i, size_t j) const
{
    return _data[j * _width  + i];
}

size_t System::Width()
{
    return _width;
}

float System::Linf()
{
    float ret = -1.0f;
    for (size_t j = 0; j < _width; j++)
    {
        for (size_t i = 0; i < _width; i++)
        {
            float x = (float)i / (float)_width;
            float y = (float)j / (float)_width;

            ret = max(ret, abs(_analyticSolution(x, y) - operator()(i, j)));
        }
    }

    return ret;
}

float System::L1()
{
    float ret = 0.0f;
    for (size_t j = 0; j < _width; j++)
    {
        for (size_t i = 0; i < _width; i++)
        {
            float x = (float)i / (float)_width;
            float y = (float)j / (float)_width;

            ret += abs(_analyticSolution(x, y) - operator()(i, j));
        }
    }

    return ret / ((float)(_width * _width));
}

float System::L2()
{
    float ret = 0.0f;
    for (size_t j = 0; j < _width; j++)
    {
        for (size_t i = 0; i < _width; i++)
        {
            float x = (float)i / (float)_width;
            float y = (float)j / (float)_width;

            ret += pow(_analyticSolution(x, y) - operator()(i, j), 2.0f);
        }
    }

    return sqrt(ret / ((float)(_width * _width)));
}

float System::_analyticSolution(float x, float y)
{
    return sin(x * 2.0f * PI) * sin(y * 2.0f * PI) * pow(E, -8.0f * D * PI * PI * t);
}
